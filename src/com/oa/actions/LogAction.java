package com.oa.actions;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.oa.models.TDevice;
import com.oa.models.TLog;
import com.oa.models.TUser;
import com.oa.services.TDeviceServices;
import com.oa.services.TLogServices;
import com.oa.services.TUserServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 功能：记录管理
 *
 */
public class LogAction extends BaseAction{
	private TLogServices logServices;
	private TDeviceServices deviceServices;
	private TUserServices userServices;
	private Integer id;
	private String device;
	private String userid;
	private String maintainid;
	private String inputdate;
	private String startdate;
	private String enddate;
	private String searchname;
	private String msg;
	private String num;
	private String status;
	private String error;
	
	
	public String queryHTLog() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		if(getError()!=null&&"0".equals(getError())){
			setError("操作成功！");
		}else if(getError()!=null&&"1".equals(getError())){
			setError("操作失败，库存不足！");
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		cond.append(" and a.status= '0' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "log/log_queryHTLog";
		PageInfo pageInfo = this.logServices.queryTLog(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		
		return "queryHTLog";
	}
	public String queryHTLog3() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		if(getError()!=null&&"0".equals(getError())){
			setError("操作成功！");
		}else if(getError()!=null&&"1".equals(getError())){
			setError("操作失败，库存不足！");
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		cond.append(" and a.status= '1' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "log/log_queryHTLog3";
		PageInfo pageInfo = this.logServices.queryTLog(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		
		return "queryHTLog3";
	}
	public String queryHTLog1() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		if(getError()!=null&&"0".equals(getError())){
			setError("操作成功！");
		}else if(getError()!=null&&"1".equals(getError())){
			setError("操作失败，库存不足！");
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.maintainid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		cond.append(" and a.status= '0' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "log/log_queryHTLog1";
		PageInfo pageInfo = this.logServices.queryTLog(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		
		return "queryHTLog1";
	}
	public String queryHTLog2() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		if(getError()!=null&&"0".equals(getError())){
			setError("操作成功！");
		}else if(getError()!=null&&"1".equals(getError())){
			setError("操作失败，库存不足！");
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&"1".equals(((TUser)getSessionAttribute("user")).getPower())){
			cond.append(" and a.maintainid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		cond.append(" and a.status= '1' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "log/log_queryHTLog2";
		PageInfo pageInfo = this.logServices.queryTLog(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		
		return "queryHTLog2";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delTLog() throws Exception{
		logServices.delTLog(id);
		return "delTLog";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String preaddLog1() throws Exception{
		List<TDevice> devices = deviceServices.queryTDevice();
		setRequestAttribute("devices",devices);
		List<TUser> users = userServices.queryTUser();
		setRequestAttribute("devices",devices);
		setRequestAttribute("users",users);
		return "preaddLog1";
	}
	public String preaddLog2() throws Exception{
		List<TDevice> devices = deviceServices.queryTDevice();
		setRequestAttribute("devices",devices);
		List<TUser> users = userServices.queryTUser();
		setRequestAttribute("devices",devices);
		setRequestAttribute("users",users);
		return "preaddLog2";
	}
	public String addLog1() throws Exception{
		TDevice deviceTDevice = this.deviceServices.getTDevice(Integer.parseInt(device));
			boolean flag = false;
			if(status!=null&&"0".equals(status)){
				if(deviceTDevice.getAllsice()>Integer.parseInt(num)){
					deviceTDevice.setAllsice(deviceTDevice.getAllsice()-Integer.parseInt(num));
					flag = true;
				}else{
					setError("1");
				}
			}else if(status!=null&&"1".equals(status)){
				flag = true;
				deviceTDevice.setAllsice(deviceTDevice.getAllsice()+Integer.parseInt(num));
			}
			if(flag){
				deviceServices.updateTDevice(deviceTDevice);
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				TLog log = new TLog();
				log.setCreateTime(dateFormat.format(date));
				log.setTDevice(new TDevice(Integer.parseInt(device)));
				log.setNum(Integer.parseInt(num));
				log.setStatus(status);
				log.setMsg(msg);
				log.setUserid(((TUser)getSessionAttribute("user")));
				log.setMaintainid(((TUser)getSessionAttribute("user")));
				logServices.addTLog(log);
				setError("0");
			}
		return "addLog1";
	}
	public String addLog2() throws Exception{
		TDevice deviceTDevice = this.deviceServices.getTDevice(Integer.parseInt(device));
			boolean flag = false;
			if(status!=null&&"0".equals(status)){
				if(deviceTDevice.getAllsice()>Integer.parseInt(num)){
					deviceTDevice.setAllsice(deviceTDevice.getAllsice()-Integer.parseInt(num));
					flag = true;
				}else{
					setError("1");
				}
			}else if(status!=null&&"1".equals(status)){
				flag = true;
				deviceTDevice.setAllsice(deviceTDevice.getAllsice()+Integer.parseInt(num));
			}
			if(flag){
				deviceServices.updateTDevice(deviceTDevice);
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				TLog log = new TLog();
				log.setCreateTime(dateFormat.format(date));
				log.setTDevice(new TDevice(Integer.parseInt(device)));
				log.setNum(Integer.parseInt(num));
				log.setStatus(status);
				log.setMsg(msg);
				log.setUserid(((TUser)getSessionAttribute("user")));
				log.setMaintainid(((TUser)getSessionAttribute("user")));
				logServices.addTLog(log);
				setError("0");
			}
		return "addLog2";
	}
	public TLogServices getLogServices() {
		return logServices;
	}
	public void setLogServices(TLogServices logServices) {
		this.logServices = logServices;
	}
	public TDeviceServices getDeviceServices() {
		return deviceServices;
	}
	public void setDeviceServices(TDeviceServices deviceServices) {
		this.deviceServices = deviceServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getMaintainid() {
		return maintainid;
	}
	public void setMaintainid(String maintainid) {
		this.maintainid = maintainid;
	}
	public String getInputdate() {
		return inputdate;
	}
	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public TUserServices getUserServices() {
		return userServices;
	}
	public void setUserServices(TUserServices userServices) {
		this.userServices = userServices;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
}
