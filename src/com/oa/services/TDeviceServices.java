package com.oa.services;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TDevice;
import com.oa.utils.PageInfo;



/**
 * 
 * 
 * 功能：配件管理
 *
 */
public interface TDeviceServices {
	//查询
	public PageInfo queryTDevice(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Serializable addTDevice(TDevice type);
	//修改
	public Boolean updateTDevice(TDevice type);
	//ID查询
	public TDevice getTDevice(Integer id);
	//删除
	public TDevice delTDevice(Integer id);
	
	public List<TDevice> queryTDevice();
}
