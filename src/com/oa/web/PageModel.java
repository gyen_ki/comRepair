package com.oa.web;

import java.util.List;

/**
 * ��ҳ���
 * 
 * @author Administrator
 * 
 */
public class PageModel {

	private int totalRecords;

	private List list;

	private int pageNo ;

	private int pageSize;

	public PageModel(){};
	
	public PageModel(int pageSize){
		this.pageSize=pageSize;
	}
	
	public int getTotalRecords() {
		return totalRecords;
	}
	

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		if (pageNo <= 0) {
			return 1;
		}
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getTotalPages() {
		return (totalRecords + pageSize - 1) / pageSize;
	}
	

}
