<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

%>
<!-- 
功能介绍：系统登录界面

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>计算机维护维修管理系统</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
     var path = "<%=request.getContextPath()%>";
     var basePath = "<%=basePath%>";
    </script>
<script type="text/javascript">
function commit() {
	if(form1.username.value=="") {
		alert("请您输入用户名！");
		form1.username.focus();
		return false;
	}
	if(form1.password.value=="") {
		alert("请您输入密码！");
		form1.password.focus();
		return false;
	}
	if(form1.rand.value=="") {
		alert("请您输入验证码！");
		form1.rand.focus();
		return false;
	}
	return true;
}
function register(){
	window.location.href="/deviceweb/user_preaddTUser";
}
</script>
</head>
<body>
<form action="user_checkUser" method="post" name="form1" onsubmit="return commit()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="147" background="images/top02.gif" width="100%" ><img src="images/top.png" width="100%" /></td>
  </tr>
</table>
<center>
<fieldset style="width:562px;align=center">
<table width="562" border="0" align="center" cellpadding="0" cellspacing="0" class="right-table03">
  <tr>
    <td width="221"><table width="95%" border="0" cellpadding="0" cellspacing="0" class="login-text01">
      
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="login-text01">
          <tr>
            <td align="center"><img src="images/ico13.gif" width="107" height="97" /></td>
          </tr>
          <tr>
            <td height="40" align="center">&nbsp;</td>
          </tr>
          
        </table></td>
        <td><img src="images/line01.gif" width="5" height="292" /></td>
      </tr>
    </table></td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="31%" height="35" class="login-text02">帐	号&nbsp;&nbsp;&nbsp;<br /></td>
        <td width="69%"><input name="username" id= "username" type="text" size="28" style="width:150px" /></td>
      </tr>
      <tr>
        <td height="35" class="login-text02">密	码&nbsp;&nbsp;&nbsp;<br /></td>
        <td><input name="password" id="password" type="password" size="30" style="width:150px"/></td>
      </tr>
      <tr>
        <td height="35" class="login-text02">权	限&nbsp;&nbsp;&nbsp;<br /></td>
        <td><select name="power">
        					<option  value="0" >管理员</option>
					    	<option  value="1" >员工</option>
					    	<option  value="2" >客户</option>
					    	<option  value="3" >商家</option>
					    </select></td>
      </tr>
      <tr>
        <td height="35" class="login-text02">验证码&nbsp;&nbsp;&nbsp;<br /></td>
        <td><img border=0 src="/deviceweb/image.jsp" ><input name="rand" id="rand" type="text" size="15" style="width:88px"/></td>
      </tr>
      <tr>
        <td height="35">&nbsp;</td>
        <td><input name="Submit2" type="submit" class="right-button02" value="登 录" />
          &nbsp;&nbsp;<input name="reset232" type="reset" class="right-button02" value="重 置" />
          &nbsp;&nbsp;<input name="reset232" onclick="register();" type="button"" class="right-button02" value="注 册" />
      </tr>      
      <%
      if(request.getAttribute("error")!=null){ %>
      	<font color="red"><%=request.getAttribute("error") %></font>
      <%}%>
      
    </table></td>
  </tr>
</table>
</fieldset>
</center>
</form>
</body>
</html>