package com.oa.actions;


import org.apache.struts2.ServletActionContext;

import com.oa.models.TType;
import com.oa.services.TTypeServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 类型管理
 *
 */
public class TypeAction extends BaseAction{
	private TTypeServices typeServices;
	private Integer id;
	private String typename;
	private String searchname;
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public String queryType() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		cond.append(" and a.isdel = 0 ");
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.typename like '%"+searchname.trim()+"%' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "deviceweb/type_queryType";
		PageInfo pageInfo = this.typeServices.queryTType(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		
		return "queryType";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addType() throws Exception{
		TType type = new TType();
		type.setTypename(typename);
		type.setIsdel("0");
		typeServices.addTType(type);
		return "addType";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateType() throws Exception{
		TType type = typeServices.getTType(id);
		setRequestAttribute("type",type);
		return "preupdateType";
	}
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateType() throws Exception{
		
		TType type = this.typeServices.getTType(id);
		type.setTypename(typename);
		typeServices.updateTType(type);
		return "updateType";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delType() throws Exception{
		TType type = typeServices.getTType(id);
		type.setIsdel("1");
		typeServices.updateTType(type);
		return "delType";
	}

	
	
	
	public TTypeServices getTypeServices() {
		return typeServices;
	}
	public void setTypeServices(TTypeServices typeServices) {
		this.typeServices = typeServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}

}
