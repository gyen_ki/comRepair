package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TRepair;
import com.oa.utils.PageInfo;



/**
 * 
 * 
 * 功能：记录管理
 *
 */
public interface TRepairServices {
	//查询
	public PageInfo queryTRepair(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTRepair(TRepair type);
	//修改
	public Boolean updateTRepair(TRepair type);
	//ID查询
	public TRepair getTRepair(Integer id);
	//删除
	public TRepair delTRepair(Integer id);
	
	public List<TRepair> queryTRepair();
}
