/*
SQLyog Enterprise - MySQL GUI v7.0 RC
MySQL - 5.5.20 : Database - devicedb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`devicedb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `devicedb`;

/*Table structure for table `t_department` */

DROP TABLE IF EXISTS `t_department`;

CREATE TABLE `t_department` (
  `id` int(4) NOT NULL,
  `departmentname` varchar(50) DEFAULT NULL,
  `isdel` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `t_department` */

insert  into `t_department`(`id`,`departmentname`,`isdel`) values (1,'施工一部','0'),(2,'施工二部','0'),(3,'施工三部','0'),(4,'后勤部门1','0');

/*Table structure for table `t_device` */

DROP TABLE IF EXISTS `t_device`;

CREATE TABLE `t_device` (
  `id` int(4) NOT NULL,
  `devicename` varchar(200) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `allsice` int(10) DEFAULT NULL,
  `num` varchar(50) DEFAULT NULL,
  `typeid` int(4) DEFAULT NULL,
  `isdel` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_device` (`typeid`),
  CONSTRAINT `FK_t_device` FOREIGN KEY (`typeid`) REFERENCES `t_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `t_device` */

insert  into `t_device`(`id`,`devicename`,`remark`,`allsice`,`num`,`typeid`,`isdel`) values (1,'配件1','配件1',106,'001',1,'0'),(2,'配件2','配件2',1,'002',1,'0'),(3,'配件3','配件3',30,'003',3,'0'),(4,'配件4','12345',205,'12345',4,'0'),(5,'配件5','5',200,'11133',4,'0');

/*Table structure for table `t_input` */

DROP TABLE IF EXISTS `t_input`;

CREATE TABLE `t_input` (
  `id` int(4) NOT NULL,
  `deviceId` int(4) DEFAULT NULL,
  `createTime` varchar(50) DEFAULT NULL,
  `userId` int(4) DEFAULT NULL,
  `startTime` varchar(20) DEFAULT NULL,
  `num` varchar(50) DEFAULT NULL,
  `status` varchar(5) DEFAULT NULL,
  `endTime` varchar(20) DEFAULT NULL,
  `isdel` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_input` */

insert  into `t_input`(`id`,`deviceId`,`createTime`,`userId`,`startTime`,`num`,`status`,`endTime`,`isdel`) values (5,1,'2017-01-10',1,'2017-01-10','D20160305184233844','出库','2017-01-10','0'),(6,5,'2017-01-10',4,'2017-01-10','D20160305194258578','出库','2017-01-10','0'),(7,1,'2017-01-10',NULL,'2017-01-10','D20160309220711828','入库',NULL,'0');

/*Table structure for table `t_log` */

DROP TABLE IF EXISTS `t_log`;

CREATE TABLE `t_log` (
  `id` int(4) NOT NULL,
  `deviceid` int(4) DEFAULT NULL,
  `userid` int(50) DEFAULT NULL,
  `maintainid` int(50) DEFAULT NULL,
  `num` int(10) DEFAULT NULL,
  `createTime` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `msg` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_log` (`deviceid`),
  CONSTRAINT `FK_t_log` FOREIGN KEY (`deviceid`) REFERENCES `t_device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `t_log` */

insert  into `t_log`(`id`,`deviceid`,`userid`,`maintainid`,`num`,`createTime`,`status`,`msg`) values (1,4,7,7,200,'2017-01-10 20:40:37','1','2'),(2,5,8,8,200,'2017-01-10 20:44:35','1','200'),(3,5,5,5,5,'2017-01-10 20:46:04','0','5');

/*Table structure for table `t_repair` */

DROP TABLE IF EXISTS `t_repair`;

CREATE TABLE `t_repair` (
  `id` int(4) NOT NULL,
  `deviceid` int(4) DEFAULT NULL,
  `userid` int(4) DEFAULT NULL,
  `maintainid` int(4) DEFAULT NULL,
  `num` varchar(100) DEFAULT NULL,
  `createTime` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `msg` varchar(1000) DEFAULT NULL,
  `baojia` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_repair` */

insert  into `t_repair`(`id`,`deviceid`,`userid`,`maintainid`,`num`,`createTime`,`status`,`msg`,`baojia`) values (2,2,3,2,'1233','2017-01-10 19:36:48','3','1233','100'),(3,3,3,NULL,'1231','2017-01-10 19:13:22','0','1231','100'),(4,5,5,8,'4321','2017-01-10 20:48:03','3','4321','100');

/*Table structure for table `t_type` */

DROP TABLE IF EXISTS `t_type`;

CREATE TABLE `t_type` (
  `id` int(4) NOT NULL,
  `typename` varchar(50) DEFAULT NULL,
  `isdel` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `t_type` */

insert  into `t_type`(`id`,`typename`,`isdel`) values (1,'日常配件','0'),(2,'工作配件','0'),(3,'后勤配件','0'),(4,'后勤配件1','0');

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(4) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `power` varchar(50) DEFAULT NULL,
  `departmentid` int(4) DEFAULT NULL,
  `isdel` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_user` (`departmentid`),
  CONSTRAINT `FK_t_user` FOREIGN KEY (`departmentid`) REFERENCES `t_department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`username`,`password`,`realname`,`power`,`departmentid`,`isdel`) values (1,'admin','a','管理员1','0',NULL,'0'),(2,'xiaoli','a','小李1','1',3,'0'),(3,'xiaowang','a','小王1','2',2,'0'),(4,'xiaosong','a','小宋1','3',3,'0'),(5,'xiaowang1','a','小旺','3',3,'0'),(6,'xiaoli1','a','晓丽','3',3,'1'),(7,'xiaohou','a','小红','1',3,'0'),(8,'xiaowa','a','小瓦','1',4,'0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
