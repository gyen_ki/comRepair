package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TUser;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 功能：用户管理接口
 *
 */
public interface TUserServices {
	//查询
	public PageInfo queryTUser(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTUser(TUser user);
	//修改
	public Boolean updateTUser(TUser user);
	//ID查询
	public TUser getTUser(Integer id);
	//删除
	public TUser delTUser(Integer id);
	
	public List<TUser> queryTUser();
	
	public TUser checkUser(String username, String password, String power);
	public TUser checkUser(String username);
}
