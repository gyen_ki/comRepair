﻿<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：添加

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<SCRIPT type="text/javascript">
function checkValue(){
				if(document.mainForm.num.value==""||document.mainForm.num.value==null)
				{
					alert("不能为空！");
					document.mainForm.num.focus();
					return false;
				}
				return true;
		}

</SCRIPT>
</head>

<body class="ContentBody">
  <form action="repair_addRepair" method="post"  name="mainForm" id="mainForm" onSubmit="return checkValue()" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;添加</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>配件信息</legend>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">配件名称:</td>
					    <td width="35%"><span class="red">
					    <select name="device">
					    	<c:forEach items="${devices }" var="s">
					    	<option  value="${s.id }">${s.devicename } </option>
					    	</c:forEach>
					    </select>
				        *</span>
				        </td>
					  </tr>
					  <%--<tr>
					    <td align="right" width="19%">使用人:</td>
					    <td width="35%"><span class="red">
					    <select name="maintainid">
					    	<c:forEach items="${users }" var="s">
					    	<option  value="${s.id }">${s.realname } </option>
					    	</c:forEach>
					    </select>
				        *</span>
				        </td>--%>
					  </tr>
					  <tr>
					    <td align="right" width="19%">状态:</td>
					    <td width="35%">
					    报修
				         <input name='status' type="hidden" value="0"/>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">维修编号:</td>
					    <td width="35%"><span class="red">
				        <input name='num' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					   <tr>
					    <td align="right" width="19%">备注:</td>
					    <td width="35%"><span class="red">
					    <textarea name="msg" rows="4" cols="60"></textarea>
				        *</span>
				        </td>
					  </tr>
					  </table>
			  <br />
				</fieldset>			
			</td>
		</tr>
		</table>
	 </td>
  </tr>
	<tr>
		<td colspan="2" align="center" height="50px">
		<input type="submit" name="submitbut" value="保存" class="button" />　
		
		<input type="reset" name="reset" value="重置" class="button"  />
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>
