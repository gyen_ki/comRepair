package com.oa.services.impl;


import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TDevice;
import com.oa.services.TDeviceServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;

/**
 * 
 * 
 * 功能：配件管理接口实现类
 *
 */
public class TDeviceServicesImpl extends AbstractServices implements TDeviceServices {
	/**
	 * 查询
	 */
	public PageInfo queryTDevice(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getTDevicesCount(cond);
		List<TDevice> list = this.getTDevices(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 获得
	 *
	 */
	public int getTDevicesCount(String cond) {
		try {
			String hql = " select count(a) from TDevice a where a.isdel=0 and  1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public List<TDevice> getTDevices(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TDevice a where a.isdel=0 and  1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Serializable addTDevice(TDevice type) {
		try {
			return saveReturn(type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 获得
	 */
	public TDevice getTDevice(Integer id) {
		
		return (TDevice)load(TDevice.class, id);
	}
	/**
	 * 删除
	 */
	public TDevice delTDevice(Integer id) {
		deleteById(TDevice.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTDevice(TDevice type) {
		update(type);
		return null;
	}

	public List<TDevice> queryTDevice() {
		String hql = " from TDevice a where a.isdel=0 and  1=1  ";
		return getResultList(hql);
	}

}
