package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TUser;
import com.oa.services.TUserServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;


public class TUserServicesImpl extends AbstractServices implements TUserServices {
	/**
	 * 查询
	 */
	public PageInfo queryTUser(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getTUsersCount(cond);
		List<TUser> list = this.getTUsers(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 获得
	 *
	 */
	public int getTUsersCount(String cond) {
		try {
			String hql = " select count(a) from TUser a where a.isdel=0 and  1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
		}
		return 0;
	}
	public List<TUser> getTUsers(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TUser a where a.isdel=0 and  1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addTUser(TUser user) {
		save(user);
		return true;
	}
	/**
	 * 获得
	 */
	public TUser getTUser(Integer id) {
		
		return (TUser)load(TUser.class, id);
	}
	/**
	 * 删除
	 */
	public TUser delTUser(Integer id) {
		deleteById(TUser.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTUser(TUser user) {
		update(user);
		return null;
	}

	public List<TUser> queryTUser() {
		String hql = " from TUser a where a.power !=0 and a.isdel=0 and  1=1  ";
		return getResultList(hql);
	}

	
	/**
	 * 
	 *
	 * 功能：会员登录验证
	 *
	 */
	public TUser checkUser(String username, String password, String power) {

		String wherejpql = "from TUser o where o.isdel=0 and  o.username =? and o.password = ? and o.power = ? ";

		List<TUser> list = getResultList(wherejpql,new Object[] {username, password, power});

		if (list.size() == 1) {
			return (TUser)list.get(0);
		}
		return null;
	}
	public TUser checkUser(String username) {

		String wherejpql = "from TUser o where o.isdel=0 and   o.username =? ";

		List<TUser> list = getResultList(wherejpql,new Object[] {username});

		if (list.size() == 1) {
			return (TUser)list.get(0);
		}
		return null;
	}
}
