package com.oa.actions;

import java.util.List;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.oa.models.TUser;
import com.oa.services.TDepartmentServices;
import com.oa.services.TUserServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 功能：用户管理
 *
 */
public class UserAction extends BaseAction{
	private TUserServices userServices;
	private TDepartmentServices departmentServices;
	private Integer id;
	private String department;
	private String username;
	private String password;
	private String realname;
	private String power;
	private String topower;
	private String searchname;
	private String rand;
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public String queryTUser() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		cond.append(" and a.isdel = 0 ");
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.username like '%"+searchname.trim()+"%' ");
		}
		if(null!=topower&&!"".equals(topower.trim())){
			cond.append(" and a.power ='"+topower.trim()+"' ");
		}
		cond.append(" and a.power != 0  ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "deviceweb/user_queryTUser";
		PageInfo pageInfo = this.userServices.queryTUser(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("topower", topower);
		setRequestAttribute("searchname", this.searchname);
		if("1".equals(topower)){
			return "queryTUser1";
		}else if("2".equals(topower)){
			return "queryTUser2";
		}else{
			return "queryTUser3";
		}
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String preaddTUser() throws Exception{
		List department  = departmentServices.queryTDepartment();
		setRequestAttribute("department",department);
		return "preaddTUser";
	}
	
	public String addTUser() throws Exception{
		TUser userTUser = userServices.checkUser(username);
		if(userTUser==null){
			TUser user = new TUser();
			user.setPassword(password);
			user.setPower(power);
			user.setRealname(realname);
			user.setIsdel("0");
			user.setTDepartment(departmentServices.getTDepartment(Integer.parseInt(department)));
			user.setUsername(username);
			userServices.addTUser(user);
			setRequestAttribute("error", "创建成功！");
		}else{
			setRequestAttribute("error", "创建失败，用户名已经存在！");
		}
		return "addTUser";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateTUser() throws Exception{
		List department  = departmentServices.queryTDepartment();
		setRequestAttribute("department",department);
		TUser user = userServices.getTUser(id);
		setRequestAttribute("user",user);
		setRequestAttribute("topower", topower);
		return "preupdateTUser";
	}
	public String preupdateMyTUser() throws Exception{
		TUser user = userServices.getTUser(((TUser)getSessionAttribute("user")).getId());
		setRequestAttribute("user",user);
		return "preupdateMyTUser";
	}
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateTUser() throws Exception{
		TUser user = this.userServices.getTUser(id);
		user.setPassword(password);
		user.setPower(power);
		user.setRealname(realname);
		user.setTDepartment(departmentServices.getTDepartment(Integer.parseInt(department)));
		userServices.updateTUser(user);
		setRequestAttribute("topower", topower);
		return "updateTUser";
	}
	public String updateMyTUser() throws Exception{
		TUser user = this.userServices.getTUser(id);
		user.setPassword(password);
		user.setRealname(realname);
		userServices.updateTUser(user);
		return "updateMyTUser";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delTUser() throws Exception{
		TUser type = userServices.getTUser(id);
		type.setIsdel("1");
		setRequestAttribute("topower", topower);
		userServices.updateTUser(type);
		return "delTUser";
	}
	
	
	public String checkUser() throws Exception{
		if (!(StringUtils.isEmpty(username)|| StringUtils.isEmpty(password)|| StringUtils.isEmpty(rand))|| StringUtils.isEmpty(power)) {
			if (getSessionAttribute("rand").equals(rand)) {
				TUser user = userServices.checkUser(username,password,power);
				if (user!=null) {
					setSessionAttribute("user", user);
					return SUCCESS;
				}
				setRequestAttribute("error", "帐号或密码错误！");
			}else{
				setRequestAttribute("error", "验证码错误！");
			}
		}
		return ERROR;
	}
	
	public TUserServices getUserServices() {
		return userServices;
	}
	public void setUserServices(TUserServices userServices) {
		this.userServices = userServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	public TDepartmentServices getDepartmentServices() {
		return departmentServices;
	}
	public void setDepartmentServices(TDepartmentServices departmentServices) {
		this.departmentServices = departmentServices;
	}
	public String getTopower() {
		return topower;
	}
	public void setTopower(String topower) {
		this.topower = topower;
	}
	public String getRand() {
		return rand;
	}
	public void setRand(String rand) {
		this.rand = rand;
	}
	
}
