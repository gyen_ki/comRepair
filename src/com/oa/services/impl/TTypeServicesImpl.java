package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TType;
import com.oa.services.TTypeServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;

/**
 * 
 * 
 * 功能：类型管理接口实现类
 *
 */
public class TTypeServicesImpl extends AbstractServices implements TTypeServices {
	/**
	 * 查询
	 */
	public PageInfo queryTType(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getTTypesCount(cond);
		List<TType> list = this.getTTypes(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 获得
	 *
	 */
	public int getTTypesCount(String cond) {
		try {
			String hql = " select count(a) from TType a where a.isdel=0 and  1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public List<TType> getTTypes(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TType a where a.isdel=0 and  1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addTType(TType type) {
		save(type);
		return true;
	}
	/**
	 * 获得
	 */
	public TType getTType(Integer id) {
		
		return (TType)load(TType.class, id);
	}
	/**
	 * 删除
	 */
	public TType delTType(Integer id) {
		deleteById(TType.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTType(TType type) {
		update(type);
		return null;
	}

	public List<TType> queryTType() {
		String hql = " from TType a where a.isdel=0 and  1=1  ";
		return getResultList(hql);
	}

}
