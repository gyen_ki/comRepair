package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TDepartment;
import com.oa.utils.PageInfo;


/**
 * 
 * 部门
 *
 */

public interface TDepartmentServices {
	//查询
	public PageInfo queryTDepartment(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTDepartment(TDepartment phone);
	//修改
	public Boolean updateTDepartment(TDepartment phone);
	//ID查询
	public TDepartment getTDepartment(Integer id);
	//删除
	public TDepartment delTDepartment(Integer id);
	
	public List<TDepartment> queryTDepartment();
}
