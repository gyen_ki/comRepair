<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：修改

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<SCRIPT type="text/javascript">
function checkValue(){
				if(document.mainForm.devicename.value==""||document.mainForm.devicename.value==null)
				{
					alert("不能为空！");
					document.mainForm.devicename.focus();
					return false;
				}
				if(document.mainForm.remark.value==""||document.mainForm.remark.value==null)
				{
					alert("不能为空！");
					document.mainForm.remark.focus();
					return false;
				}
				if(document.mainForm.num.value==""||document.mainForm.num.value==null)
				{
					alert("不能为空！");
					document.mainForm.num.focus();
					return false;
				}
				if(document.mainForm.allsice.value==""||document.mainForm.allsice.value==null)
				{
					alert("不能为空！");
					document.mainForm.allsice.focus();
					return false;
				}
				return true;
		}

</SCRIPT>
</head>

<body class="ContentBody">
  <form action="device_updateDevice" method="post"  name="mainForm" id="mainForm" onSubmit="return checkValue()" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;修改</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>配件信息</legend>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">配件名称:</td>
					    <td width="35%"><span class="red">
					    <input name='id' type="hidden" class="text" style="width:354px" value="<s:property value="#request.device.id"/>"/>
				        <input name='devicename' type="text" class="text" style="width:354px" value="<s:property value="#request.device.devicename"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					  <tr>
					    <td align="right" width="19%">配件编号:</td>
					    <td width="35%"><span class="red">
				        <input name='num' type="text" class="text" style="width:354px" value="<s:property value="#request.device.num"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">配件库存:</td>
					    <td width="35%"><span class="red">
				        <input name='allsice' type="text" class="text" style="width:354px" value="<s:property value="#request.device.allsice"/>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">备注:</td>
					    <td width="35%"><span class="red">
					    <textarea name="remark" rows="4" cols="60"><s:property value="#request.device.remark"/></textarea>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">类型:</td>
					    <td width="35%"><span class="red">
					    <select name="type">
					    	<c:forEach items="${type }" var="s">
					    	<option  value="${s.id }" <c:if test="${s.id==device.TType.id }"> selected="selected" </c:if> >${s.typename } </option>
					    	</c:forEach>
					    </select>
				        *</span>
				        </td>
					  </tr>
					  </table>
			  <br />
				</fieldset>			
			</td>
		</tr>
		</table>
	 </td>
  </tr>
	<tr>
		<td colspan="2" align="center" height="50px">
		<input type="submit" name="submitbut" value="保存" class="button" />　
		
		<input type="reset" name="reset" value="重置" class="button"  />
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>
