package com.oa.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Test {
	/**
	 * 每隔100条做一个保存点
	 * 事物回滚的时候回到指定保存点
	 * SavePoint
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		long l1 = System.currentTimeMillis();
		Connection con = null;
		con = DB.getConnection();
		Statement stmt = con.createStatement();
		for(int i=0;i<=1000;i++) {
			String sql = "insert into t_organization values('"+i+"','+"+i+"+哈哈+','嘿嘿','"+i+"',null)";
			stmt.addBatch(sql);
			if(i%10==0) {
				stmt.executeBatch();
				stmt.clearBatch();
			}
		}
		long l2 = System.currentTimeMillis();
		System.out.println("此次插入用时-----"+(l2-l1)+"mms");
		stmt.close();
		con.close();
	}

}
