package com.oa.actions;

import org.apache.struts2.ServletActionContext;

import com.oa.models.TDepartment;
import com.oa.services.TDepartmentServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 部门管理
 *
 */
public class DepartmentAction extends BaseAction{
	private TDepartmentServices departmentServices ;
	private Integer id;
	private String departmentname;
	private String searchname;
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public String queryTDepartment() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		cond.append(" and a.isdel = 0 ");
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.departmentname like '%"+searchname.trim()+"%' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "deviceweb/department_queryTDepartment";
		PageInfo pageInfo = this.departmentServices.queryTDepartment(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		
		return "queryTDepartment";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String addTDepartment() throws Exception{
		TDepartment department = new TDepartment();
		department.setDepartmentname(departmentname);
		department.setIsdel("0");
		departmentServices.addTDepartment(department);
		return "addTDepartment";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateTDepartment() throws Exception{
		TDepartment department = departmentServices.getTDepartment(id);
		setRequestAttribute("department",department);
		return "preupdateTDepartment";
	}
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateTDepartment() throws Exception{
		TDepartment department = this.departmentServices.getTDepartment(id);
		department.setDepartmentname(departmentname);
		departmentServices.updateTDepartment(department);
		return "updateTDepartment";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delTDepartment() throws Exception{
		TDepartment department = departmentServices.getTDepartment(id);
		department.setIsdel("1");
		departmentServices.updateTDepartment(department);
		return "delTDepartment";
	}
	public TDepartmentServices getDepartmentServices() {
		return departmentServices;
	}
	public void setDepartmentServices(TDepartmentServices departmentServices) {
		this.departmentServices = departmentServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}

	
}
