package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.oa.models.TDepartment;
import com.oa.services.TDepartmentServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;

/**
 * 
 * 
 * 功能：部门管理接口实现类
 *
 */
public class TDepartmentServicesImpl extends AbstractServices implements TDepartmentServices {
	/**
	 * 查询
	 */
	public PageInfo queryTDepartment(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getTDepartmentsCount(cond);
		List<TDepartment> list = this.getTDepartments(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 获得
	 *
	 */
	public int getTDepartmentsCount(String cond) {
		try {
			String hql = " select count(a) from TDepartment a where a.isdel=0 and 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public List<TDepartment> getTDepartments(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TDepartment a where a.isdel=0 and  1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addTDepartment(TDepartment type) {
		save(type);
		return true;
	}
	/**
	 * 获得
	 */
	public TDepartment getTDepartment(Integer id) {
		
		return (TDepartment)load(TDepartment.class, id);
	}
	/**
	 * 删除
	 */
	public TDepartment delTDepartment(Integer id) {
		deleteById(TDepartment.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTDepartment(TDepartment type) {
		update(type);
		return null;
	}

	public List<TDepartment> queryTDepartment() {
		String hql = " from TDepartment a where a.isdel=0 and  1=1  ";
		return getResultList(hql);
	}


}
