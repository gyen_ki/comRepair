package com.oa.services.impl;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TRepair;
import com.oa.services.TRepairServices;
import com.oa.utils.PageInfo;
import com.oa.web.AbstractServices;

/**
 * 
 * 
 * 功能：记录接口实现类
 *
 */
public class TRepairServicesImpl extends AbstractServices implements TRepairServices {
	/**
	 * 查询
	 */
	public PageInfo queryTRepair(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond) {
		int rowCount = this.getTRepairsCount(cond);
		List<TRepair> list = this.getTRepair(currentpage,
				pageunit, cond);
		PageInfo PageInfo = new PageInfo(currentpage, pageunit, rowCount, url,
				list);
		return PageInfo;
	}
	
	/**
	 * 
	 *
	 * 获得
	 *
	 */
	public int getTRepairsCount(String cond) {
		try {
			String hql = " select count(a) from TRepair a where 1=1 "
					+ cond;
			Object t = this.queryAggregation(hql);
			return ((Long) t).intValue();
		} catch (Exception e) {
		}
		return 0;
	}
	public List<TRepair> getTRepair(int currentpage, int pageunit,
			String cond) {

		try {
			String hql = " from TRepair a where 1=1  " + cond;
			return this.query(hql, currentpage, pageunit);
		} catch (Exception e) {
		}
		return null;
	}
	/**
	 * 添加
	 */
	public Boolean addTRepair(TRepair phone) {
		save(phone);
		return true;
	}
	/**
	 * 获得
	 */
	public TRepair getTRepair(Integer id) {
		
		return (TRepair)load(TRepair.class, id);
	}
	/**
	 * 删除
	 */
	public TRepair delTRepair(Integer id) {
		deleteById(TRepair.class, id);
		return null;
	}
	/**
	 * 修改
	 */
	public Boolean updateTRepair(TRepair phone) {
		update(phone);
		return null;
	}

	public List<TRepair> queryTRepair() {
		String hql = " from TRepair a where 1=1  ";
		return getResultList(hql);
	}

}
