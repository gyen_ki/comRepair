package com.oa.actions;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.oa.models.TDevice;
import com.oa.models.TRepair;
import com.oa.models.TUser;
import com.oa.services.TDeviceServices;
import com.oa.services.TRepairServices;
import com.oa.services.TUserServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 功能：记录管理
 *
 */
public class RepairAction extends BaseAction{
	private TRepairServices repairServices;
	private TDeviceServices deviceServices;
	private TUserServices userServices;
	private Integer id;
	private String device;
	private String userid;
	private String maintainid;
	private String inputdate;
	private String startdate;
	private String enddate;
	private String searchname;
	private String msg;
	private String num;
	private String status;
	private String error;
	private String baojia;
	
	
	public String queryHTRepair() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.userid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "repair/repair_queryHTRepair";
		PageInfo pageInfo = this.repairServices.queryTRepair(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryHTRepair";
	}
	public String queryHTRepair4() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.userid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "repair/repair_queryHTRepair4";
		PageInfo pageInfo = this.repairServices.queryTRepair(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryHTRepair4";
	}
	public String queryHTRepair1() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.userid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		cond.append(" and a.status = '0' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "repair/repair_queryHTRepair1";
		PageInfo pageInfo = this.repairServices.queryTRepair(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryHTRepair1";
	}
	public String queryHTRepair2() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.userid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		cond.append(" and a.status = '1' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "repair/repair_queryHTRepair2";
		PageInfo pageInfo = this.repairServices.queryTRepair(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryHTRepair2";
	}
	public String queryHTRepair3() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.userid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		cond.append(" and a.status = '2' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "repair/repair_queryHTRepair3";
		PageInfo pageInfo = this.repairServices.queryTRepair(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryHTRepair3";
	}
	public String queryHTRepair0() throws Exception{
		if (getSessionAttribute("querypageunit") == null) {
			setSessionAttribute("querypageunit",this.pageunit);
		}
		StringBuffer cond = new StringBuffer();
		if(null!=searchname&&""!=searchname.trim()){
			cond.append(" and a.TDevice.devicename like '%"+searchname.trim()+"%' ");
		}
		if(null!=getSessionAttribute("user")&&("3".equals(((TUser)getSessionAttribute("user")).getPower())||"2".equals(((TUser)getSessionAttribute("user")).getPower()))){
			cond.append(" and a.userid.id= '"+((TUser)getSessionAttribute("user")).getId()+"' ");
		}
		cond.append(" and a.status = '0' ");
		int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
		int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

		String url = "repair/repair_queryHTRepair0";
		PageInfo pageInfo = this.repairServices.queryTRepair(curpage,
				pageunit, ServletActionContext.getRequest(), url, cond.toString());
		setRequestAttribute("pageinfo", pageInfo);
		setRequestAttribute("searchname", this.searchname);
		return "queryHTRepair0";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String preupdateRepair() throws Exception{
		setRequestAttribute("repair", repairServices.getTRepair(id));
		return "preupdateRepair";
	}
	public String delTRepair() throws Exception{
		repairServices.delTRepair(id);
		return "delTRepair";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String setRepair() throws Exception{
		TRepair repair = repairServices.getTRepair(id);
		repair.setBaojia(baojia);
		repairServices.updateTRepair(repair);
		return "setRepair";
	}
	public String updateRepair() throws Exception{
		TRepair repair = repairServices.getTRepair(id);
		repair.setStatus(status);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		repair.setCreateTime(dateFormat.format(date));
		String url = "preaddRepair";
		if("1".equals(status)){
			repair.setMaintainid(((TUser)getSessionAttribute("user")));
			url = "preaddRepair1";
		}
		if("2".equals(status)){
			repair.setMaintainid(((TUser)getSessionAttribute("user")));
			url = "preaddRepair2";
		}
		if("3".equals(status)){
			repair.setUserid(((TUser)getSessionAttribute("user")));
			url = "preaddRepair3";
		}
		repairServices.updateTRepair(repair);
		return url;
	}
	public String preaddRepair() throws Exception{
		List<TDevice> devices = deviceServices.queryTDevice();
		setRequestAttribute("devices",devices);
		List<TUser> users = userServices.queryTUser();
		setRequestAttribute("devices",devices);
		setRequestAttribute("users",users);
		return "preaddRepair";
	}
	public String addRepair() throws Exception{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		TRepair repair = new TRepair();
		repair.setCreateTime(dateFormat.format(date));
		repair.setTDevice(new TDevice(Integer.parseInt(device)));
		repair.setNum(num);
		repair.setStatus("0");
		repair.setMsg(msg);
		repair.setUserid(((TUser)getSessionAttribute("user")));
		//repair.setMaintainid(((TUser)getSessionAttribute("user")));
		repairServices.addTRepair(repair);
		setError("0");
		return "addRepair";
	}
	public TRepairServices getRepairServices() {
		return repairServices;
	}
	public void setRepairServices(TRepairServices repairServices) {
		this.repairServices = repairServices;
	}
	public TDeviceServices getDeviceServices() {
		return deviceServices;
	}
	public void setDeviceServices(TDeviceServices deviceServices) {
		this.deviceServices = deviceServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getMaintainid() {
		return maintainid;
	}
	public void setMaintainid(String maintainid) {
		this.maintainid = maintainid;
	}
	public String getInputdate() {
		return inputdate;
	}
	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public TUserServices getUserServices() {
		return userServices;
	}
	public void setUserServices(TUserServices userServices) {
		this.userServices = userServices;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getBaojia() {
		return baojia;
	}
	public void setBaojia(String baojia) {
		this.baojia = baojia;
	}
	
}
