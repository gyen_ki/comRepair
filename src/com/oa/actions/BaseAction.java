package com.oa.actions;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {
	public String currentpage = "1";
	public String pageunit = "10";
	

	public String getCurrentpage() {
		return currentpage;
	}

	public void setCurrentpage(String currentpage) {
		this.currentpage = currentpage;
	}

	public String getPageunit() {
		return pageunit;
	}

	public void setPageunit(String pageunit) {
		this.pageunit = pageunit;
	}
	public String getCurrentpage(HttpServletRequest request) {
		String currentpage = request.getParameter("currentpage");
		if(currentpage != null && !"".equals(currentpage)){
			return currentpage;
		}
		return this.currentpage;
	}

	public String getPageunit(HttpServletRequest request,String str) {
		String pageunit= request.getParameter("pageunit");
		if(pageunit != null && !"".equals(pageunit)){
			request.getSession().setAttribute(str, pageunit);
			return pageunit;
		}else{
			return request.getSession().getAttribute(str).toString();
		}
	}

	/**
	 * @return HttpServletRequest
	 */
	public HttpServletRequest getHttpServletRequest() {

		return ServletActionContext.getRequest();

	}

	/**
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getRequestMap() {
		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> requestMap = actionContext.getContextMap();
		return requestMap;
	}

	/**
	 * @param name
	 * @return Object
	 */
	public Object getRequestAttribute(String name) {
		return getRequestMap().get(name);
	}

	/**
	 * @param name
	 * @param value
	 * @return void
	 */
	public void setRequestAttribute(String name, Object value) {

		getRequestMap().put(name, value);
	}

	/**
	 * @param name
	 * @return String
	 */
	public String getRequestParameter(String name) {

		if (getRequestParameterMap().get(name)!=null) {
			return getHttpServletRequest().getParameter(name);
		} else {
			return StringUtils.EMPTY;
		}
	}

	/**
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getRequestParameterMap() {
		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> parameterMap = actionContext.getParameters();
		return parameterMap;
	}

	/**
	 * 
	 * 
	 * 
	 * @param name
	 * @return String[]
	 */
	public String[] getParameterValues(String name) {
		return getHttpServletRequest().getParameterValues(name);
	}

	/**
	 * @return HttpSession
	 */
	public HttpSession getHttpSession() {
		return getHttpServletRequest().getSession();
	}

	/**
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getSessionMap() {

		ActionContext actionContext = ActionContext.getContext();
		Map<String, Object> sessionMap = actionContext.getSession();
		return sessionMap;
	}

	/**
	 * @param name
	 * @return Object
	 */
	public Object getSessionAttribute(String name) {
		return getSessionMap().get(name);
	}

	/**
	 * 
	 * 
	 * @param name
	 * @param value
	 * @return void
	 * @throws
	 */
	public void setSessionAttribute(String name, Object value) {
		getSessionMap().put(name, value);
	}

	/**
	 * 
	 * @return HttpServletResponse
	 */
	public HttpServletResponse getHttpServletResponse() {
		return ServletActionContext.getResponse();
	}

	/**
	 * 
	 * @return ServletContext
	 */
	public ServletContext getApplication() {
		return ServletActionContext.getServletContext();
	}

	/**
	 * 
	 * 
	 * @Description: AJAX���
	 * 
	 * @param content
	 * @param type
	 * @return String
	 * @throws
	 */
	public String ajax(String content, String type) {
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType(type + ";charset=UTF-8");
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			response.getWriter().write(content);
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 */
	public String ajaxText(String text) {
		return ajax(text, "text/plain");
	}

	/**
	 */
	public String ajaxHtml(String html) {
		return ajax(html, "text/html");
	}

	/**
	 */
	public String ajaxXml(String xml) {
		return ajax(xml, "text/xml");
	}

	/**
	 */
	public String ajaxJson(String jsonString) {
		return ajax(jsonString, "text/html");
	}




	/**
	 * 
	 * 
	 * @return void
	 * @throws
	 */
	public void setResponseNoCache() {
		getHttpServletResponse().setHeader("progma", "no-cache");
		getHttpServletResponse().setHeader("Cache-Control", "no-cache");
		getHttpServletResponse().setHeader("Cache-Control", "no-store");
		getHttpServletResponse().setDateHeader("Expires", 0);
	}



	private static final long serialVersionUID = 4711141009520101116L;
	public static final String VIEW = "view";
	public static final String LIST = "list";
	public static final String STATUS = "status";
	public static final String WARN = "warn";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String MESSAGE = "message";
	
	protected String id;
	protected String[] ids;

}
