package com.oa.models;

/**
 * TUser generated by MyEclipse Persistence Tools
 */

public class TUser implements java.io.Serializable {

	// Fields

	private Integer id;
	private TDepartment TDepartment;
	private String username;
	private String password;
	private String realname;
	private String power;
	private String isdel;

	// Constructors

	public String getIsdel() {
		return isdel;
	}

	public void setIsdel(String isdel) {
		this.isdel = isdel;
	}

	/** default constructor */
	public TUser() {
	}

	/** minimal constructor */
	public TUser(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public TUser(Integer id, TDepartment TDepartment, String username,
			String password, String realname, String power) {
		this.id = id;
		this.TDepartment = TDepartment;
		this.username = username;
		this.password = password;
		this.realname = realname;
		this.power = power;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TDepartment getTDepartment() {
		return this.TDepartment;
	}

	public void setTDepartment(TDepartment TDepartment) {
		this.TDepartment = TDepartment;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealname() {
		return this.realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getPower() {
		return this.power;
	}

	public void setPower(String power) {
		this.power = power;
	}

}