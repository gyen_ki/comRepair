package com.oa.actions;


import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.oa.models.TDevice;
import com.oa.models.TType;
import com.oa.services.TDeviceServices;
import com.oa.services.TLogServices;
import com.oa.services.TTypeServices;
import com.oa.utils.PageInfo;

/**
 * 
 * 
 * 配件管理
 *
 */
public class DeviceAction extends BaseAction{
	private TDeviceServices deviceServices;
	private TTypeServices typeServices;
	private TLogServices logServices;
	private Integer id;
	private String type;
	private String devicename;
	private String remark;
	private String allsice;
	private String num;
	private String status;
	private String searchname;
	private String maintainid;
	/**
	 * 
	 *
	 * 查询
	 *
	 */
	public String queryDevice() {
		try {
			if (getSessionAttribute("querypageunit") == null) {
				setSessionAttribute("querypageunit",this.pageunit);
			}
			StringBuffer cond = new StringBuffer();
			cond.append(" and a.isdel = 0 ");
			if(null!=searchname&&""!=searchname.trim()){
				cond.append(" and a.devicename like '%"+searchname.trim()+"%' ");
			}
			int curpage = Integer.parseInt(this.getCurrentpage(ServletActionContext.getRequest()));
			int pageunit = Integer.parseInt(this.getPageunit(ServletActionContext.getRequest(), "querypageunit"));

			String url = "deviceweb/device_queryDevice";
			PageInfo pageInfo = this.deviceServices.queryTDevice(curpage,
					pageunit, ServletActionContext.getRequest(), url, cond.toString());
			setRequestAttribute("pageinfo", pageInfo);
			setRequestAttribute("searchname", this.searchname);
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "queryDevice";
	}
	/**
	 * 
	 *
	 * 添加
	 *
	 */
	public String preaddDevice() throws Exception{
		List<TType> type = typeServices.queryTType();
		setRequestAttribute("type",type);
		return "preaddDevice";
	}
	public String addDevice() throws Exception{
		TDevice device = new TDevice();
		device.setDevicename(devicename);
		device.setAllsice(Integer.parseInt(allsice));
		device.setNum(num);
		device.setRemark(remark);
		device.setIsdel("0");
		device.setAllsice(Integer.parseInt(allsice));
		device.setTType(typeServices.getTType(Integer.parseInt(getRequestParameter("type"))));
		deviceServices.addTDevice(device);
		return "addDevice";
	}
	/**
	 * 
	 *
	 * 预修改
	 *
	 */
	public String preupdateDevice() throws Exception{
		List<TType> type = typeServices.queryTType();
		setRequestAttribute("type",type);
		TDevice device = deviceServices.getTDevice(id);
		setRequestAttribute("device",device);
		return "preupdateDevice";
	}
	/**
	 * 
	 *
	 * 修改
	 *
	 */
	public String updateDevice() throws Exception{
		TDevice device = this.deviceServices.getTDevice(id);
		device.setDevicename(devicename);
		device.setNum(num);
		device.setRemark(remark);
		device.setAllsice(Integer.parseInt(allsice));
		device.setTType(typeServices.getTType(Integer.parseInt(getRequestParameter("type"))));
		deviceServices.updateTDevice(device);
		return "updateDevice";
	}
	/**
	 * 
	 *
	 * 删除
	 *
	 */
	public String delDevice() throws Exception{
		TDevice device = deviceServices.getTDevice(id);
		device.setIsdel("1");
		deviceServices.updateTDevice(device);
		return "delDevice";
	}
	public TDeviceServices getDeviceServices() {
		return deviceServices;
	}
	public void setDeviceServices(TDeviceServices deviceServices) {
		this.deviceServices = deviceServices;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDevicename() {
		return devicename;
	}
	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getAllsice() {
		return allsice;
	}
	public void setAllsice(String allsice) {
		this.allsice = allsice;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getSearchname() {
		return searchname;
	}
	public void setSearchname(String searchname) {
		this.searchname = searchname;
	}
	public TTypeServices getTypeServices() {
		return typeServices;
	}
	public void setTypeServices(TTypeServices typeServices) {
		this.typeServices = typeServices;
	}
	public TLogServices getLogServices() {
		return logServices;
	}
	public void setLogServices(TLogServices logServices) {
		this.logServices = logServices;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMaintainid() {
		return maintainid;
	}
	public void setMaintainid(String maintainid) {
		this.maintainid = maintainid;
	}
}
