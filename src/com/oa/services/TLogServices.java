package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TLog;
import com.oa.utils.PageInfo;



/**
 * 
 * 
 * 功能：记录管理
 *
 */
public interface TLogServices {
	//查询
	public PageInfo queryTLog(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTLog(TLog type);
	//修改
	public Boolean updateTLog(TLog type);
	//ID查询
	public TLog getTLog(Integer id);
	//删除
	public TLog delTLog(Integer id);
	
	public List<TLog> queryTLog();
}
