package com.oa.db;

import java.sql.*;

public class DB {
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static Connection getConnection () throws SQLException {
		Connection con = null;
		con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/devicedb?useUnicode=true&characterEncoding=utf-8", "root", "123456");
		return con;
	}
	
		
	public static void main(String[] args) {
		try {
			System.out.println(DB.getConnection());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

