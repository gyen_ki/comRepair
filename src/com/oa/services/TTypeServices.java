package com.oa.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.oa.models.TType;
import com.oa.utils.PageInfo;



/**
 * 
 * 
 * 功能：类型管理
 *
 */
public interface TTypeServices {
	//查询
	public PageInfo queryTType(int currentpage, int pageunit,
			HttpServletRequest request, String url, String cond);
	//添加
	public Boolean addTType(TType type);
	//修改
	public Boolean updateTType(TType type);
	//ID查询
	public TType getTType(Integer id);
	//删除
	public TType delTType(Integer id);
	
	public List<TType> queryTType();
}
