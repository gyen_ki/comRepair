<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg"  uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：类型信息

 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.tabfont01 {	
	font-family: "宋体";
	font-size: 9px;
	color: #555555;
	text-decoration: none;
	text-align: center;
}
.font051 {font-family: "宋体";
	font-size: 12px;
	color: #333333;
	text-decoration: none;
	line-height: 20px;
}
.font201 {font-family: "宋体";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.button {
	font-family: "宋体";
	font-size: 14px;
	height: 37px;
}
html { overflow-x: auto; overflow-y: auto; border:0;} 
-->
</style>

<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="mainForm" id="mainForm" method="post" action="log_queryHTLog">

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="62" background="images/nav04.gif">
          
		   <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
			<td width="21"><img src="images/ico07.gif" width="20" height="18" /></td>
			<td >查看内容： 按配件名称：<input type="text" name="searchname" value="<s:property value="#request.searchname"/>" />           
              <input name="Submit" type="submit" class="right-button02" value="查 询" /></td>
		  </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table id="subtree1" style="DISPLAY: " width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
          <td><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">

          	 <tr>
               <td height="20"><font color="blue"  >查询配件领用信息</font><%
      if(request.getAttribute("error")!=null){ %>
      	<font color="red"><%=request.getAttribute("error") %></font>
      <%}%></td>
          	 </tr>
          	 <tr>
               <td height="10" valign="top" ><hr size="1" color="green" /></td>
          	 </tr>
              <tr>
                <td height="40" class="font42">
                <table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" >
                  <tr>
				    <td  align="center" bgcolor="#EEEEEE">序列</td>
				    <td  align="center" bgcolor="#EEEEEE">配件名称</td>
				    <td  align="center" bgcolor="#EEEEEE">配件编号</td>
				    <td  align="center" bgcolor="#EEEEEE">操作时间</td>
				    <td  align="center" bgcolor="#EEEEEE">数量</td>
				    <td  align="center" bgcolor="#EEEEEE">操作人</td>
				    <td  align="center" bgcolor="#EEEEEE">录入人</td>
				    <td  align="center" bgcolor="#EEEEEE">状态</td>
				    <td  align="center" bgcolor="#EEEEEE">备注</td>
                  </tr>
                  <s:iterator value="#request.pageinfo.datas" var="list" status="i">
	                  <tr>
	                    <td bgcolor="#FFFFFF" width="5%" height="20" align="center" ><s:property value="#i.index+1"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.TDevice.devicename"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.TDevice.num"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.createTime"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.num"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.maintainid.realname"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.userid.realname"/></td>
					    <td bgcolor="#FFFFFF"  align="center"><c:if test="${list.status==0}">领用</c:if><c:if test="${list.status==1}">入库</c:if></td>
					    <td bgcolor="#FFFFFF"  align="center"><s:property value="#list.msg"/></td>
	                  </tr>
	                </s:iterator>
                </table></td>
              </tr>
              <tr>
              	<td>
              	<jsp:include flush="true" page="../../pagetag.jsp"></jsp:include>
              	</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
</form>
</body>
</html>
